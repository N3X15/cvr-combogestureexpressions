﻿using ABI.CCK.Components;
using UnityEditor;
using UnityEngine;

namespace Hai.ComboGesture.Scripts.Editor.Internal
{
    public class VisemeAnimationMaker
    {
        private readonly CVRAvatar _avatar;

        public VisemeAnimationMaker(CVRAvatar avatar)
        {
            _avatar = avatar;
        }

        public void OverrideAnimation(AnimationClip animationToMutate, int visemeNumber, float amplitude)
        {
            var faceMesh = _avatar.bodyMesh;
            var path = ResolveRelativePath(_avatar.transform, faceMesh.transform);
            for (var currentViseme = 0; currentViseme < 15; currentViseme++)
            {
                var binding = EditorCurveBinding.FloatCurve(
                    path,
                    typeof(SkinnedMeshRenderer),
                    "blendShape." + _avatar.visemeBlendshapes[currentViseme]
                );

                var currentAmplitude = currentViseme == visemeNumber ? amplitude * 100 : 0;
                AnimationUtility.SetEditorCurve(animationToMutate, binding, new AnimationCurve(
                    new Keyframe(0, currentAmplitude),
                    new Keyframe(1 / 60f, currentAmplitude)
                ));
            }
        }

        private static string ResolveRelativePath(Transform avatar, Transform item)
        {
            if (item.parent != avatar && item.parent != null)
            {
                return ResolveRelativePath(avatar, item.parent) + "/" + item.name;
            }

            return item.name;
        }
    }
}
