﻿using System.Collections.Generic;
using System.Linq;
using ABI.CCK.Components;
using UnityEngine;

namespace Hai.ComboGesture.Scripts.Editor.Internal
{
    public class BlendshapesFinder
    {
        private readonly CVRAvatar _avatar;

        public BlendshapesFinder(CVRAvatar avatar)
        {
            _avatar = avatar;
        }

        public List<BlendShapeKey> FindBlink()
        {
            if (_avatar == null)
            {
                return new List<BlendShapeKey>();
            }

            var eyeLook = _avatar.bodyMesh;
            if (eyeLook == null || eyeLook.sharedMesh == null)
            {
                return new List<BlendShapeKey>();
            }

            var relativePathToSkinnedMesh = SharedLayerUtils.ResolveRelativePath(_avatar.transform, eyeLook.transform);
            return _avatar.blinkBlendshape
                .Where(blendShapeName => blendShapeName != null)
                .Select(blendShapeName => new BlendShapeKey(relativePathToSkinnedMesh, blendShapeName))
                .ToList();
        }

        public List<BlendShapeKey> FindLipsync()
        {
            if (_avatar == null)
            {
                return new List<BlendShapeKey>();
            }

            if (_avatar.visemeMode != CVRAvatar.CVRAvatarVisemeMode.Visemes
                || _avatar.bodyMesh == null
                || _avatar.bodyMesh.sharedMesh == null)
            {
                return new List<BlendShapeKey>();
            }

            var relativePathToSkinnedMesh = SharedLayerUtils.ResolveRelativePath(_avatar.transform, _avatar.bodyMesh.transform);
            return _avatar.blinkBlendshape
                .Where(blendShapeName => blendShapeName != null)
                .Select(blendShapeName => new BlendShapeKey(relativePathToSkinnedMesh, blendShapeName))
                .ToList();
        }
    }

    public readonly struct BlendShapeKey
    {
        public BlendShapeKey(string path, string blendShapeName)
        {
            Path = path;
            BlendShapeName = blendShapeName;
        }

        public CurveKey AsCurveKey()
        {
            return new CurveKey(this.Path, typeof(SkinnedMeshRenderer), "blendShape." + this.BlendShapeName);
        }

        public string Path { get; }
        public string BlendShapeName { get; }

        public static bool operator ==(BlendShapeKey left, BlendShapeKey right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(BlendShapeKey left, BlendShapeKey right)
        {
            return !left.Equals(right);
        }

        public bool Equals(BlendShapeKey other)
        {
            return Path == other.Path && BlendShapeName == other.BlendShapeName;
        }

        public override bool Equals(object obj)
        {
            return obj is BlendShapeKey other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Path != null ? Path.GetHashCode() : 0) * 397) ^ (BlendShapeName != null ? BlendShapeName.GetHashCode() : 0);
            }
        }
    }
}
