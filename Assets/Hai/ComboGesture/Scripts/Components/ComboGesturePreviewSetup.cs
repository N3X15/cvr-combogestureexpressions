﻿using System.Collections.Generic;
using ABI.CCK.Components;
using Hai.ExpressionsEditor.Scripts.Components;
using UnityEngine;

namespace Hai.ComboGesture.Scripts.Components
{
    public class ComboGesturePreviewSetup : ExpressionEditorPreviewable
    {
        public Camera camera;
        public Animator previewDummy;
        public bool autoHide;
        public CVRAvatar avatarDescriptor;

        public override bool IsValid()
        {
            return false;
        }

        public override EePreviewAvatar AsEePreviewAvatar()
        {
            return new EePreviewAvatar(
                new List<EePreviewAvatarCamera> { new EePreviewAvatarCamera("(Main Camera)", camera.gameObject, true, HumanBodyBones.Head, null, null) },
                previewDummy,
                autoHide,
                gameObject.name,
                avatarDescriptor.bodyMesh,
                new List<EePreviewAvatarPose>()
            );
        }

        public override GameObject AsGameObject()
        {
            return gameObject;
        }
    }
}
