# ComboGestureExpressions (CGE) for ChilloutVR CCK 3.7

## Forked

This project has been forked from version 1.5.3 of [Hai's original repository](https://github.com/hai-vr/combo-gesture-expressions-av3) for use in ChilloutVR.

I am not affiliated with Hai-VR or their team.  Do not go to them if shit breaks, it's probably my fault.

While I am currently a CVR developer, this project is a personal project and is not condoned by, affiliated with, nor a product of Alpha Blend Interactive UG or any of its partners.

**WARNING:** Right now, this project is functional, but causes very bad animator bugs for some reason. **DO NOT USE YET.**

* [Releases](https://gitlab.com/N3X15/cvr-combogestureexpressions/-/releases)

## Old README I Need To Fix

*ComboGestureExpressions* is an Unity Editor tool that lets you attach face expressions to hand gestures and take as much advantage of *Avatars 3.0*'s quality of life features.

<!-- ![](https://gitlab.com/N3X15/CVR-ComboGestureExpressions/raw/z-res-pictures/Documentation/illustration-1.gif) -->

A common issue with classic avatars are face expressions that conflict when both hands are combined.
For instance, if a face expression closes the eyes on the left hand, but lowers the eyelids on the other hand, the face will look wrong.

*ComboGestureExpressions* takes advantage of Avatars 3.0 animators to fix this using predictable combo animations, but also introduces new features that will expand the expressions of your avatar:

* Using the expressions menu, attach multiple expressions on a single gesture by switching between entire sets of face expressions representing different moods.
* Eyes will no longer blink whenever the avatar has a face expression with eyes closed.
* Puppets and blend trees are integrated into the tool, with automatic correction of incorrect blending.
* ...and more tweaks.

This tool should NOT be used for:

* ❌ Animating hand and finger positions.
  To animate hand and finger positions, use the Avatars 3.0's Gesture layer which is made for this purpose.

## Documentation / Tutorial

Full documentation complete with illustrative videos and video tutorials [is available here](https://hai-vr.github.io/combo-gesture-expressions-av3/).

<!-- [![](https://github.com/hai-vr/combo-gesture-expressions-av3/raw/z-res-pictures/Documentation/documentation.png)](https://hai-vr.github.io/combo-gesture-expressions-av3/) -->

*[Open Documentation / Tutorial at https://hai-vr.github.io/combo-gesture-expressions-av3/ ...](https://hai-vr.github.io/combo-gesture-expressions-av3/)*
